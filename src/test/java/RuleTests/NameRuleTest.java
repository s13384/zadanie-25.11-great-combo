package RuleTests;

import org.junit.Test;

import RuleBase.CheckResult;
import RuleBase.Person;
import RuleBase.RuleResult;
import Rules.NameRule;
import junit.framework.Assert;

public class NameRuleTest {

public NameRule rule = new NameRule();
	
	@Test
	public void checker_should_check_if_the_person_name_is_not_null(){
		Person p = new Person();
		CheckResult result = rule.checkRule(p);
		Assert.assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_name_is_not_empty(){
		Person p = new Person();
		p.setFirstName("");
		CheckResult result = rule.checkRule(p);
		Assert.assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_name_is_not_null(){
		Person p = new Person();
		p.setFirstName("Jan");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		
	}
	@Test
	public void checker_should_return_error_if_string_contains_numbers(){
		Person p = new Person();
		p.setFirstName("Jan9");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
		
	}
}
