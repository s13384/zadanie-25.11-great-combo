package RuleBase;

import java.util.ArrayList;

public class RuleChecker<TEntity> {
	
	private ArrayList<ICanCheckRule<TEntity>> rules;

	public ArrayList<ICanCheckRule<TEntity>> getRules() {
		return rules;
	}

	public void setRules(ArrayList<ICanCheckRule<TEntity>> rules) {
		this.rules = rules;
	}
	
	public ArrayList<CheckResult> check(TEntity entity){
		ArrayList<CheckResult> result  = new ArrayList<CheckResult>();
		
		for(ICanCheckRule<TEntity> rule: rules){
			result.add(rule.checkRule(entity));
		}
		return result;
	}
}
