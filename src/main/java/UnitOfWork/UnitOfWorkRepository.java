package UnitOfWork;

import TableModels.EntityAbstractClass;

public interface UnitOfWorkRepository {

	void persistAdd(EntityAbstractClass entity);
	void persistDelete(EntityAbstractClass entity);
	void persistUpdate(EntityAbstractClass entity);
}
