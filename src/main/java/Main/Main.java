package Main;

import java.sql.Connection;
import java.sql.DriverManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;

import RuleBase.Person;

public class Main {

	public static void main(String[] args) {
		/*
		 try {
		        Class.forName("org.hsqldb.jdbcDriver" );
		        Connection c = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/xdb", "sa", "");
		    } catch (Exception e) {
		        System.out.println("ERROR: failed to load HSQLDB JDBC driver.");
		        e.printStackTrace();
		        return;
		    }
		*/
		DateTime date = new DateTime();
		date.withYear(1980);
		date.withMonthOfYear(8);
		date.withDayOfMonth(10);
		Person p = new Person();
		p.setDateOfBirth(date);
		p.setEmail("person_person@company.person.com");
		p.setFirstName("firstName");
		p.setLogin("person-login");
		p.setNip("00000000000");
		p.setPassword("password");
		p.setPesel("80811012345");
		p.setSurname("surname");
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(p);
		session.getTransaction().commit();
		
	}

}
