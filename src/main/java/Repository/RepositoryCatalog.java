package Repository;

public interface RepositoryCatalog {

	EnumerationValueRepository enumerations();
	UserRepository users();
}
