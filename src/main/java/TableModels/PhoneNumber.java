package TableModels;

public class PhoneNumber extends EntityAbstractClass{

	
	private int countryPrefix;
	private int cityPrefix;
	private String number;
	private int typeId;
	private int personId;
	
	
	public int getCountryPrefix() {
		return countryPrefix;
	}
	public void setCountryPrefix(int countryPrefix) {
		this.countryPrefix = countryPrefix;
	}
	public int getCityPrefix() {
		return cityPrefix;
	}
	public void setCityPrefix(int cityPrefix) {
		this.cityPrefix = cityPrefix;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
}
