package TableModels;

public enum EntityState {

	New,
	Modified,
	UnChanged,
	Deleted,
	Unknown;
}
