package TableModels;

import java.util.ArrayList;

import javax.persistence.Entity;

import org.joda.time.DateTime;

@Entity
public class Person extends EntityAbstractClass{
	
	private String firstName;
	private String surName;
	private String pesel;
	private String nip;
	private String email;
	private DateTime dateOfBirth;
	private ArrayList<PhoneNumber> phoneNumbers;
	private ArrayList<Adress> adresses;
	
	public Person(){
		
	}
	public Person(String firstName, String surName, String pesel, String nip){
		this.firstName = firstName;
		this.surName = surName;
		this.pesel = pesel;
		this.nip = nip;
	}
	public Person(int id, EntityState state, String firstName, String surName, String pesel, String nip){
		this.id = id;
		this.state = state;
		this.firstName = firstName;
		this.surName = surName;
		this.pesel = pesel;
		this.nip = nip;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public DateTime getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(DateTime dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public void addPhoneNumber(PhoneNumber phoneNumber){
		phoneNumbers.add(phoneNumber);
	}
	public ArrayList<PhoneNumber> getPhoneNumbers(){
		return phoneNumbers;
	}
	public ArrayList<PhoneNumber> getPhoneNumbersWithTypeId(int typeId){
		ArrayList<PhoneNumber> tmp = new ArrayList<PhoneNumber>();
		for (PhoneNumber phNum : phoneNumbers){
			if (phNum.getTypeId()==typeId){
				tmp.add(phNum);
			}
		}
		return tmp;
	}

}
