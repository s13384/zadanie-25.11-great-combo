package Rules;

import RuleBase.CheckResult;
import RuleBase.ICanCheckRule;
import RuleBase.Person;
import RuleBase.RuleResult;

public class PeselRule implements ICanCheckRule<Person> {

	@Override
	public CheckResult checkRule(Person entity) {
		int suma = 0, tmp = 1;
		for (int i = 0 ; i<10 ; i++){
			try {
			suma += tmp * Character.getNumericValue((entity.getPesel().charAt(i)));
			} catch (Exception e){
				// jak pesel ma litery to jest zly
				return new CheckResult("",RuleResult.Error);
			}
			if (tmp==1)
				tmp = 3;
			else if (tmp==3)
				tmp = 7;
			else if (tmp==7)
				tmp = 9;
			else if (tmp==9)
				tmp = 1;
		}
		suma = suma%10;
		if (suma != 0){
			suma = 10 - suma;
		}
		if (suma == Character.getNumericValue((entity.getPesel().charAt(10))))
			return new CheckResult("",RuleResult.Ok);
		else
			return new CheckResult("",RuleResult.Error);
	}

}
