package Rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import RuleBase.CheckResult;
import RuleBase.ICanCheckRule;
import RuleBase.Person;
import RuleBase.RuleResult;

public class EmailRule implements ICanCheckRule<Person>{

	@Override
	public CheckResult checkRule(Person entity) {
		//Pattern pattern = Pattern.compile("[\\S]+[@]{1}[\\S]+[\\.]{1}[a-zA-Z0-9]+");
		//Pattern pattern = Pattern.compile("[_a-zA-Z0-9\\-\\.]+@[a-zA-Z0-9_-\\.]+[\\.]{1}[a-zA-Z0-9_-\\.]+");
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		
		Matcher matcher = pattern.matcher(entity.getEmail());
		if (matcher.matches())
			return new CheckResult("", RuleResult.Ok);
		else 
			return new CheckResult("", RuleResult.Error);
	}

}
