package Rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import RuleBase.CheckResult;
import RuleBase.ICanCheckRule;
import RuleBase.Person;
import RuleBase.RuleResult;

public class PasswordRule implements ICanCheckRule<Person>{

	@Override
	public CheckResult checkRule(Person entity) {
		
		if (entity.getPassword() == null)
			return new CheckResult("Password is null", RuleResult.Error);
		
		if (entity.getPassword().length() < 8)
			return new CheckResult("", RuleResult.Error);
		
		Pattern pattern = Pattern.compile("^[0-9]{8,}$");
		Matcher matcher = pattern.matcher(entity.getPassword());
		if (matcher.matches())
			return new CheckResult("Password is very weak", RuleResult.Ok);
		
		Pattern pattern2 = Pattern.compile("^[a-zA-Z0-9]{8,}$");
		Matcher matcher2 = pattern2.matcher(entity.getPassword());
		if (matcher2.matches())
		{
			if (entity.getPassword().length() < 12)
				return new CheckResult("Password is weak", RuleResult.Ok);
			else 
				return new CheckResult("Password is okay", RuleResult.Ok);
		}
		
		Pattern pattern3 = Pattern.compile("^[\\S]{8,}$");
		Matcher matcher3 = pattern3.matcher(entity.getPassword());
		if (matcher3.matches())
		{
			if (entity.getPassword().length() < 12)
				return new CheckResult("Password is okay", RuleResult.Ok);
			else if (entity.getPassword().length() < 20)
				return new CheckResult("Password is strong", RuleResult.Ok);
			else 
				return new CheckResult("Password is very strong", RuleResult.Ok);
		}
		
		return new CheckResult("", RuleResult.Error);
	}

}
