package Rules;

import RuleBase.CheckResult;
import RuleBase.ICanCheckRule;
import RuleBase.Person;
import RuleBase.RuleResult;

public class NipRule implements ICanCheckRule<Person> {

	@Override
	public CheckResult checkRule(Person entity) {
		
		int suma=10;
		try {
		suma = countControlSum(entity);
		int lastNumberOfNip = Character.getNumericValue(entity.getNip().charAt(9));
		if (suma == lastNumberOfNip)
			return new CheckResult("",RuleResult.Ok);
		else
			return new CheckResult("",RuleResult.Error);
		} catch (Exception e){
			e.printStackTrace();
			return new CheckResult("",RuleResult.Error);
		}
	}
	private int countControlSum(Person entity){
		
		try {
		int[] suma = new int [9];
		for (int i=0;i<9;i++){
			suma[i]=Character.getNumericValue(entity.getNip().charAt(i));
		}
		int sum=6*suma[0] + 5*suma[1] + 7*suma[2] + 
				2*suma[3] + 3*suma[4] + 4*suma[5] +
				5*suma[6] + 6*suma[7] + 7*suma[8];
		sum = sum%11;
		return sum;
		}catch (Exception e){
			e.printStackTrace();
			return 10;
		}
	}

}
